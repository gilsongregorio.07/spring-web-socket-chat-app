FROM openjdk:8
EXPOSE 8080
ADD target/sample-websocket.jar sample-websocket.jar
ENTRYPOINT ["java","-jar","/sample-websocket.jar"]
